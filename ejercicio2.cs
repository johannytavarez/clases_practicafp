using System;
namespace Clases
{
    /*Crear tres clases ClaseA, ClaseB, ClaseC que ClaseB herede de ClaseA y ClaseC herede de 
    ClaseB. Definir un constructor a cada clase que muestre un mensaje. Luego definir un objeto de 
    la clase ClaseC.*/

    class ClaseA
    {
        public ClaseA()
        {
            Console.WriteLine("hola");
        }

    }
    class ClaseB:ClaseA
    {
        public ClaseB()
        {
            Console.WriteLine("como");
        }
    }

    class ClaseC:ClaseB
    {
       public ClaseC()
        {
            Console.WriteLine("estas");
        }    

        ClaseC objeto = new ClaseC();
             

    }



}