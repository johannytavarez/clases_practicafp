﻿using System;
/*Crear una clase Contacto. Esta clase deberá tener los atributos "nombre, apellidos, telefono y 
direccion". También deberá tener un método "SetContacto", de tipo void y con los parámetros string, 
que permita cambiar el valor de los atributos. También tendrá un método "Saludar", que escribirá en 
pantalla "Hola, soy " seguido de sus datos. Crear también una clase llamada ProbarContacto. 
Esta clase deberá contener sólo la función Main, que creará dos objetos de tipo Contacto, les 
asignará los datos del contacto y les pedirá que saluden.*/
namespace Clases
{
    public class Contacto
    {
        public string nombre;
        public string apellido;
        public string telefono;
        public string direccion;

       public void SetContacto () 
        {
            nombre = Console.ReadLine();
            apellido = Console.ReadLine();
            telefono = Console.ReadLine();
            direccion = Console.ReadLine();
        }

        public void Saludar () 
        {
            Console.WriteLine($"Hola, soy {nombre} {apellido}, mi numero es {telefono} y vivo en {direccion}");
        }

    }

    class PobarContacto
    {

        static void Main(string[] args)
        {
           Contacto myContacto = new Contacto();
            Console.WriteLine("Ingrese los datos del nuevo contacto. Primero nombre, leugo apellido, despues el numero y finalmente la direccion");
            myContacto.SetContacto();
           myContacto.Saludar();

           Contacto myContacto2 = new Contacto();

            Console.WriteLine("Ingrese los datos del nuevo contacto. Primero nombre, leugo apellido, despues el numero y finalmente la direccion");
           myContacto2.SetContacto();
           myContacto2.Saludar();

        }
    }
}
