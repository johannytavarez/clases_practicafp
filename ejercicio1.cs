using System;
namespace Clases{
/*Crear una clase Persona que tenga como atributos el "cedula, nombre, apellido y la edad 
(definir las propiedades para poder acceder a dichos atributos)". Definir como responsabilidad 
un método para mostrar ó imprimir. Crear una segunda clase Profesor que herede de la clase Persona. 
Añadir un atributo sueldo ( y su propiedad) y el método para imprimir su sueldo. Definir un objeto de 
la clase Persona y llamar a sus métodos y propiedades. También crear un objeto de la clase Profesor y 
llamar a sus métodos y propiedades.*/

    class Persona
    {
        public int cedula = 123456789;
        public string nombre= "Karla";
        public string apellido= "Vasquez";
        public byte edad= 22;

        public void imprimir()
        {
            Console.WriteLine($"Los datos son: Cedula:{cedula}, Nombre: {nombre}, Apellido: {apellido}, Edad: {edad}");
        }

    }

    class Profesor: Persona
    {
        int sueldo = 20000;
        public void escribir()
        {
        Console.WriteLine("El sueldo es: "+ sueldo);
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Persona myPersona = new Persona();
            Console.WriteLine("Datos de la persona");
            myPersona.imprimir();

            Profesor myProfesor = new Profesor();
            Console.WriteLine("Datos de la profesora");
            myProfesor.cedula = 12334576;
            myProfesor.nombre = "Miranda";
            myProfesor.apellido = "Rodriguez";
            myProfesor.edad = 45;
            myProfesor.imprimir();
            myProfesor.escribir();
        }
    }

}